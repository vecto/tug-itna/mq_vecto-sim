## Summary

<!-- Summarize your purpose, problems encountered, or functionality missing. -->

## Component(s) involved

<!-- GUI/CLI, simulation/IO/Hashing/Certification, Eng/Decl mode, generic data, VECTO/Trailer/Multistep, etc. -->

## Data & steps to reproduce

<!-- Provide logs, sample inputs & outputs, screenshots, and/or instructions to reproduce the issue. -->

## Versions affected:

- Windows version(s): Win10? Win11?
- VECTO versions(s): 3.x? 0.x?
- .NET version(s): ...

<!-- **Tip**: To discover your .NET version, type **[Ctrl+R] cmd [Enter]**,  
then execute `reg query "HKLM\SOFTWARE\Microsoft\Net Framework Setu  p\NDP" /s`. -->

## Additional information

<!-- Optional information, such as your organization. -->

