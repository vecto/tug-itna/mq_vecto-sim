﻿namespace TUGraz.VectoCore.Models.BusAuxiliaries.Interfaces {
	public enum AdvancedAuxiliaryMessageType
	{
		Information = 1,
		Warning = 2,
		Critical = 3
	}
}
